#import module
import json
from pdb import set_trace
from bs4.element import Tag
import requests
from bs4 import BeautifulSoup
from requests.models import Response
#retreive all pages
url = 'https://ujsportal.pacourts.us/CaseSearch' 

pennsylvania = []

class Pennsylvania_scapper(): 
    '''a class docstring'''
    @staticmethod
    def pennsylvania_case():
        '''this function fetch the case of the participant'''
        url = 'https://ujsportal.pacourts.us/CaseSearch'


        try:
            r = requests.get(url) 
        except requests.ConnectionError as e:
            print('server error')

        data =  {'first_name' : 'Jennifer' ,  'middle_name': 'Lynn',  'last_name': 'Roberts',  'dob': '02/15/1972', 'docket_type' : 'criminal' , 'case_type' : 'closed' , 'participant_role' : 'defendant' , 'Calendar_Event_Start_Date' : '' , 'Calendar_Event_End_Date' : '' , 'Calendar_Event_Type' : '' }

        import pdb;pdb.set_trace()

         #create a new resource 
        response =  requests.post(url , data = {'first_name' : 'Jennifer' ,  'middle_name': 'Lynn',  'last_name': 'Roberts',  'dob': '02/15/1972', 'docket_type' : 'criminal' , 'case_type' : 'closed' , 'participant_role' : 'defendant' , 'Calendar_Event_Start_Date' : '' , 'Calendar_Event_End_Date' : '' , 'Calendar_Event_Type' : '' })
         
        print(response.text) #get the html of the page

        soup = BeautifulSoup(response.text, 'html.parser')
        try:
            article = soup.find('div' , {'class' : 'MainContent'})
        except AttributeError:
            raise Exception("not found div tag")


        for property in article.find_all('div' , 'property'):
                first_name = property.find('div' , class_= 'field-label required').text
                last_name = property.find('div' , class_= 'field-label required').text
                dob = property.find('div' , class_= 'field-label').text
                docket_type = property.find('div' , class_= 'field-label required').text
                case_type = property.find('div' , class_= 'field-label').text
                participant_role = property.find('div' , class_= 'field-label').text

                property_info = {
                'first_name' : first_name,
                'last_name' : last_name,
                'dob' : dob,
                'docket_type ' :  docket_type,
                'case_type' :  case_type,
                'participant_role' : participant_role
                }
                pennsylvania.append( property_info )

        return json.dumps(pennsylvania) 
        
     
if __name__ == "__main__": 
    scrapper =  Pennsylvania_scapper()
    result = scrapper.pennsylvania_case() 
    if result:
        print(result)
    else:
        print("something went wrong")
    