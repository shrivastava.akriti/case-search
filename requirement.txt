# these are the requirements for the scapper pakage 

beautifulsoup4 == 4.10.0
bs4 == 0.0.1
pip == 21.2.3
py == 1.10.0
pylint == 2.8.3
pytest == 6.2.4
requests ==  2.26.0
virtualenv ==  20.7.2 
